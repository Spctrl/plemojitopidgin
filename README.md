# plemojitopidgin

plemojitopidgin is a simple script the can read a Pleroma package list for emojis and converts it into a pidgin theme. The script checks if it finds `$HOME/.purple/smileys` and puts the converted package there. You can then select the package from pidging via `Extra > Settings > Themes`.

# How to use the script

You can simply download the plemojitopidgin script and run it locally. The top of the script has configuration options. You can change loglevels, the location of the Pleroma emoji package and the location of the pidgin emoji folder. You can only run the script with one Pleroma package at a time, arrays are not possible.

```bash
#=================================================
# CONFIG
#=================================================

LOG_LEVEL="info" # "error","warning","info","debug" 
DEFAULT_PACKS_LOCATION="https://git.pleroma.social/pleroma/emoji-index/raw/master"
PIDGIN_EMOJI_FOLDER="$HOME/.purple/smileys"
```

To list all available packages
```bash
bash plemojitopidgin -l
```
To install a package
```bash
bash plemojitopidgin <packagename>
```
For more options you can run
```bash
bash plemojitopidgin -h
```
